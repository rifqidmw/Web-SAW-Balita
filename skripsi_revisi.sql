-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2021 at 08:31 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_revisi`
--

-- --------------------------------------------------------

--
-- Table structure for table `balita`
--

CREATE TABLE `balita` (
  `idbalita` int(5) NOT NULL,
  `nama_balita` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') NOT NULL,
  `nama_ayah` varchar(100) NOT NULL,
  `nama_ibu` varchar(100) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `balita`
--

INSERT INTO `balita` (`idbalita`, `nama_balita`, `tanggal_lahir`, `jenis_kelamin`, `nama_ayah`, `nama_ibu`, `alamat`) VALUES
(1, 'Rafa Aji Pratama', '2017-01-08', 'Laki-laki', 'Naruto', 'Lia', 'Mandingan'),
(2, 'Dafa Pratama', '2018-03-16', 'Laki-laki', 'Naruto', 'Lia', 'Mandingan'),
(3, 'Valentino Kusdiarto', '2017-08-01', 'Laki-laki', 'Apik', 'Dwi', 'Mandingan'),
(4, 'Dhea ', '2016-09-05', 'Perempuan', '-', 'Eva Susiowati', 'Mandingan'),
(5, 'Via Azerala', '2017-10-10', 'Perempuan', '-', 'Wasi P', 'Mayungan'),
(6, 'Alinza AK', '2017-07-08', 'Perempuan', 'Agus M', 'Dwi S', 'Mayungan'),
(7, 'Faza Maraya', '2020-06-16', 'Laki-laki', 'Sigit Tri Handono', 'Yeni Retnowati', 'Mayungan'),
(8, 'Syafiqa Asena', '2018-08-01', 'Perempuan', 'Sugino', 'Andina', 'Mandingan'),
(9, 'Zainudin', '2019-09-05', 'Laki-laki', 'Harun', 'Indah', 'Mandingan'),
(10, 'Maya Hasna Alfiani', '2017-10-10', 'Perempuan', 'Juli', 'Ida Susanti', 'Mandingan'),
(11, 'Maudy K', '2020-07-08', 'Perempuan', '-', 'Ira', 'Mayungan'),
(12, 'Kiki Satya Yudantha', '2020-06-16', 'Laki-laki', 'Margiyanto', 'Andriana', 'Mandingan'),
(13, 'Indra Agustina', '2019-08-01', 'Laki-laki', 'Agung Handayana', 'Sulisna', 'Mandingan'),
(14, 'Syahrul Hisam', '2020-09-05', 'Laki-laki', 'Andre T', 'Ina Saswita', 'Mandingan'),
(15, 'Nayla Azzahra ', '2018-08-10', 'Perempuan', 'Rudy S', 'Wiwik Handayani', 'Mayungan'),
(16, 'Nadin Aulia Rahmawati', '2019-07-22', 'Perempuan', '-', 'Septi Novita', 'Mandingan'),
(17, 'Dinda Aulia W', '2018-06-16', 'Perempuan', 'Heri', 'Anik W', 'Mandingan'),
(18, 'Fattan Al Baihaqi', '2020-08-04', 'Laki-laki', 'Nawan ', 'Eni', 'Mandingan'),
(19, 'Wawa Anzina', '2021-01-05', 'Perempuan', 'Edi', 'Cici', 'Mayungan'),
(20, 'Fadia Azzahra I', '2020-10-10', 'Perempuan', '-', 'Andri', 'Mayungan'),
(21, 'Pipit Maudin', '2020-08-08', 'Perempuan', 'Syam', '-', 'Mandingan'),
(22, 'Rendi Y', '2020-10-16', 'Laki-laki', 'Endra S', '-', 'Mayungan'),
(23, 'Aulia Oktvia', '2020-07-01', 'Perempuan', '-', 'Erna Sugiyanti', 'Mandingan'),
(24, 'Andre Yudistira Ramadhan', '2020-05-05', 'Laki-laki', '-', 'Dina Yumaroh', 'Mandingan'),
(25, 'Iyan Ikhsanu', '2019-10-13', 'Laki-laki', 'Sayono', 'Jumartini', 'Mandingan'),
(26, 'Ryusha', '2021-01-14', 'Laki-laki', 'Rifqi', 'Erika', 'Perum. Cepoko Indah'),
(27, 'Rifqi', '2021-04-01', 'Laki-laki', 'Aa', 'Be', 'Ce');

-- --------------------------------------------------------

--
-- Table structure for table `kriteriasaw`
--

CREATE TABLE `kriteriasaw` (
  `id_kriteria` int(100) NOT NULL,
  `nama_kriteria` varchar(50) NOT NULL,
  `jenis` enum('cost','benefit') NOT NULL,
  `bobot` double NOT NULL,
  `nama_parameter` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kriteriasaw`
--

INSERT INTO `kriteriasaw` (`id_kriteria`, `nama_kriteria`, `jenis`, `bobot`, `nama_parameter`) VALUES
(1, 'Kelengkapan Imunisasi', 'benefit', 0.3, 'status_imunisasi'),
(2, 'Berat Badan', 'benefit', 0.25, 'berat_badan'),
(3, 'Tinggi Badan', 'benefit', 0.25, 'tinggi_badan'),
(4, 'Lingkar Kepala', 'benefit', 0.2, 'lingkar_kepala');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `nama`, `username`, `password`, `level`) VALUES
(7, 'Admin', 'admin', 'admin', 'admin'),
(8, 'User', 'user', 'user', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `perkembangan`
--

CREATE TABLE `perkembangan` (
  `idperkembangan` int(5) NOT NULL,
  `idbalita` int(5) NOT NULL,
  `tinggi_badan` enum('Naik','Tetap','Turun') NOT NULL,
  `berat_badan` enum('Naik','Tetap','Turun') NOT NULL,
  `lingkar_kepala` enum('Baik','Cukup','Kurang Baik') NOT NULL,
  `status_imunisasi` enum('Lengkap','Kurang Lengkap','Tidak Ada') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perkembangan`
--

INSERT INTO `perkembangan` (`idperkembangan`, `idbalita`, `tinggi_badan`, `berat_badan`, `lingkar_kepala`, `status_imunisasi`) VALUES
(1, 1, 'Naik', 'Naik', 'Baik', 'Lengkap'),
(2, 2, 'Tetap', 'Naik', 'Baik', 'Lengkap'),
(3, 3, 'Naik', 'Naik', 'Baik', 'Kurang Lengkap'),
(4, 4, 'Naik', 'Naik', 'Baik', 'Kurang Lengkap'),
(5, 5, 'Tetap', 'Tetap', 'Baik', 'Kurang Lengkap'),
(6, 6, 'Naik', 'Naik', 'Baik', 'Lengkap'),
(7, 7, 'Naik', 'Naik', 'Baik', 'Kurang Lengkap'),
(8, 8, 'Naik', 'Naik', 'Baik', 'Lengkap'),
(9, 9, 'Naik', 'Turun', 'Baik', 'Kurang Lengkap'),
(10, 10, 'Turun', 'Tetap', 'Cukup', 'Kurang Lengkap'),
(11, 11, 'Naik', 'Naik', 'Baik', 'Kurang Lengkap'),
(12, 12, 'Tetap', 'Naik', 'Baik', 'Lengkap'),
(13, 13, 'Naik', 'Naik', 'Cukup', 'Kurang Lengkap'),
(14, 14, 'Naik', 'Naik', 'Baik', 'Kurang Lengkap'),
(15, 15, 'Naik', 'Naik', 'Baik', 'Kurang Lengkap'),
(16, 16, 'Naik', 'Tetap', 'Baik', 'Kurang Lengkap'),
(17, 17, 'Naik', 'Naik', 'Baik', 'Kurang Lengkap'),
(18, 18, 'Naik', 'Naik', 'Baik', 'Kurang Lengkap'),
(19, 19, 'Naik', 'Turun', 'Baik', 'Kurang Lengkap'),
(20, 20, 'Naik', 'Naik', 'Baik', 'Kurang Lengkap'),
(21, 21, 'Tetap', 'Naik', 'Baik', 'Kurang Lengkap'),
(22, 22, 'Naik', 'Tetap', 'Baik', 'Kurang Lengkap'),
(23, 23, 'Naik', 'Naik', 'Kurang Baik', 'Tidak Ada'),
(24, 24, 'Naik', 'Naik', 'Baik', 'Lengkap'),
(25, 25, 'Tetap', 'Naik', 'Baik', 'Kurang Lengkap'),
(26, 26, 'Tetap', 'Naik', 'Baik', 'Lengkap'),
(29, 27, 'Tetap', 'Turun', 'Baik', 'Tidak Ada');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `balita`
--
ALTER TABLE `balita`
  ADD PRIMARY KEY (`idbalita`);

--
-- Indexes for table `kriteriasaw`
--
ALTER TABLE `kriteriasaw`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perkembangan`
--
ALTER TABLE `perkembangan`
  ADD PRIMARY KEY (`idperkembangan`),
  ADD KEY `idbalita` (`idbalita`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `balita`
--
ALTER TABLE `balita`
  MODIFY `idbalita` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `kriteriasaw`
--
ALTER TABLE `kriteriasaw`
  MODIFY `id_kriteria` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `perkembangan`
--
ALTER TABLE `perkembangan`
  MODIFY `idperkembangan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
