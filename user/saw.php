<?php
class Saw
{
  private $db;
  function __construct()
  {
    $this->db = new PDO('mysql:host=localhost;dbname=skripsi_revisi', "root", "");
  }

  public function get_data_kriteria(){
    $stmt = $this->db->prepare("SELECT*FROM kriteriasaw ORDER BY id_kriteria");
    $stmt->execute();
    return $stmt;
  }
	public function get_alternatif_umur(){
		$stmt = $this->db->prepare("SELECT umur FROM balita ORDER BY idbalita");
		$stmt->execute();
		return $stmt;
	}

  public function get_data_balita(){
    $stmt = $this->db->prepare("SELECT idbalita, nama_balita, alamat FROM balita ORDER BY idbalita");
    $stmt->execute();
    return $stmt;
  }

  public function get_data_kriteria_id($id){
    $stmt = $this->db->prepare("SELECT*FROM kriteriasaw WHERE id_kriteria='$id' ORDER BY id_kriteria");
    $stmt->execute();
    return $stmt;
  }
 
  public function get_data_nilai_id($id){
    $stmt = $this->db->prepare("SELECT*FROM nilaisaw WHERE id_balita='$id' ORDER BY id_kriteria");
    $stmt->execute();
    return $stmt;
  }

  public function nilai_max($id){
    $stmt = $this->db->prepare("SELECT id_kriteria, MAX(nilai) AS max FROM nilaisaw WHERE id_kriteria='$id' GROUP BY id_kriteria");
    $stmt->execute();
    return $stmt;
  }

  public function nilai_min($id){
    $stmt = $this->db->prepare("SELECT id_kriteria, MIN(nilai) AS min FROM nilai_saw WHERE id_kriteria='$id' GROUP BY id_kriteria");
    $stmt->execute();
    return $stmt;
  }

  public function get_data_balita_awal(){
    $stmt = $this->db->prepare("SELECT perkembangan.*, imunisasi.status_imunisasi FROM perkembangan INNER JOIN imunisasi ON imunisasi.id_imunisasi = perkembangan.id_imunisasi");
    $stmt->execute();
    return $stmt;
  }

  public function get_data_balita_normalisasi(){
    $stmt = $this->db->prepare("SELECT * FROM perkembangan ORDER BY idbalita");
    $stmt->execute();
    return $stmt;
  }

  public function get_data_perkembangan_pembobotan(){
    $stmt = $this->db->prepare("SELECT * FROM perkembangan ORDER BY idbalita");
    $stmt->execute();
    return $stmt;
  }

  public function get_data_balita_perangkingan(){
    $stmt = $this->db->prepare("SELECT perkembangan.*, balita.nama_balita FROM perkembangan INNER JOIN balita ON balita.idbalita = perkembangan.idbalita");
    $stmt->execute();
    return $stmt;
  }

  public function get_data_max(){
    $stmt = $this->db->prepare("SELECT perkembangan.*, max(berat_badan) as maxK2, max(tinggi_badan) as maxK3, max(lingkar_kepala) as maxK4 FROM perkembangan LEFT JOIN balita ON balita.idbalita = perkembangan.idbalita
      LEFT JOIN imunisasi ON imunisasi.id_imunisasi = perkembangan.id_imunisasi");
    $stmt->execute();
    return $stmt;
  }

  public function get_data_min(){
    $stmt = $this->db->prepare("SELECT perkembangan.*, min(berat_badan) as minK2, min(tinggi_badan) as minK3, min(lingkar_kepala) as minK4 FROM perkembangan LEFT JOIN balita ON balita.idbalita = perkembangan.idbalita
      LEFT JOIN imunisasi ON imunisasi.id_imunisasi = perkembangan.id_imunisasi");
    $stmt->execute();
    return $stmt;
  }

  public function get_kategorial($kategori, $index){
    /*Jika index = 1 yang mana berarti kategori usia*/
    if ($index == 1){
      if ($kategori == "Lengkap"){
        $stmt = "3";
      } else if ($kategori == "Kurang Lengkap"){
        $stmt = "2";
      } else {
        $stmt = "1";
      }
    } 
    /*Jika index = 4 yang mana berarti kategori Lingkar Kepala*/
    else if ($index == 4){
      if ($kategori == "Baik"){
        $stmt = "3";
      } else if ($kategori == "Cukup"){
        $stmt = "2";
      } else {
        $stmt = "1";
      }
    } 
    /*Selain index 1 dan 4, berarti kategori Berat Badan dan Tinggi Badan*/
    else {
      if ($kategori == "Naik"){
        $stmt = "3";
      } else if ($kategori == "Tetap"){
        $stmt = "2";
      } else {
        $stmt = "1";
      }
    }
    return $stmt;
  }

}

 ?>
