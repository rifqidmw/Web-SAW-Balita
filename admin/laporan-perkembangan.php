<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title tab bar di web -->
  <title>Posyandu Anggrek II Mayungan</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
  <!-- import bootstrap, JQuery, dan style.css -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
  <nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
    <!-- Logo -->
    <a class="navbar-brand">
      <img src="http://indihealth.com/indihealthcom/assets/images/products/5acf891ba61c4.png" alt="Logo"
        style="width:150px;"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Navigasi bar -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <!-- Navigasi untuk dropdown Data Balita -->
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Data Balita
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="index-balita.php">Data Umum</a>
            
            <a class="dropdown-item" href="kriteria.php">Data Kriteria</a>
          </div>
        </li>
        <!-- Navigasi untuk dropdown Data Balita -->
        <li class="nav-item">
          <a class="nav-link" href="index-perkembangan.php">Data Perkembangan</a>
        </li>
        <!-- Navigasi ke Laporan Perkembangan -->
        <li class="nav-item active">
          <a class="nav-link" href="laporan-perkembangan.php">Laporan Perkembangan</a>
        </li>
      </ul>
    </div>
  </nav>

  <br><br><br><br><br>

  <!-- Memanggil file saw.php -->
  <?php
    spl_autoload_register(function($class){
      require_once $class.'.php';
    });
    $saw = new saw();
  ?>

  <!-- Tabel kriteria -->
  <div class="container">
    <h2>Kriteria</h2>
    <table class="table table-striped table-hover table-sm table-bordered">
      <thead class="thead">
        <tr align="center">
          <th>No</th>
          <th>Nama Kriteria</th>
          <th>Jenis</th>
          <th>Bobot</th>
        </tr>
      </thead>
      <tbody>
        <?php
          /*Query untuk Mengambil data kriteria pada tabel kriteriasaw dengan memanggil fungsi get_data_kriteria di file saw*/
          $kriteria = $saw->get_data_kriteria();
          /*Mengambil jumlah data kriteria yang ada di database*/
          $jml_kriteria = $kriteria->rowCount();

          /*Perulangan untuk menampilkan tiap data kriteria*/
          while ($data_kriteria = $kriteria->fetch(PDO::FETCH_ASSOC)) {
        ?>
        <tr>
          <td align="center">K<?php echo $data_kriteria['id_kriteria']; ?></td>
          <td><?php echo $data_kriteria['nama_kriteria']; ?></td>
          <td><?php echo $data_kriteria['jenis']; ?></td>
          <td align="center"><?php echo $data_kriteria['bobot']; ?></td>
        </tr>
        <?php } ?>
      <tbody>
    </table>

    <!-- Tabel Alternatif/Data Balita -->
    <hr>
    <br><br>
    <h2>Alternatif/Data Balita</h2>
    <table class="table table-striped table-hover table-sm table-bordered">

      <thead class="thead">
        <tr align="center">
          <th>No</th>
          <th>Nama Balita</th>
          <th>Alamat</th>
        </tr>
      </thead>
      <tbody>
        <?php
        /*Query untuk Mengambil data balita pada tabel balita dengan memanggil fungsi get_data_balita di file saw*/
          $balita = $saw->get_data_balita();
          /*Perulangan untuk menampilkan tiap data balita*/
          while ($data_balita = $balita->fetch(PDO::FETCH_ASSOC)) {
        ?>
        <tr>
          <td align="center">A<?php echo $data_balita['idbalita']; ?></td>
          <td><?php echo $data_balita['nama_balita']; ?></td>
          <td><?php echo $data_balita['alamat']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <hr>
    <br><br>    

    <!-- Tabel Normalisasi -->
    <h2>Normalisasi</h2>
    <table class="table table-striped table-hover table-sm table-bordered">
      <thead class="thead">

        <tr align="center">
          <th rowspan="2">Balita</th>
          <th colspan="<?php echo $jml_kriteria; ?>">Kriteria</th>
        </tr>

        </tr>

        <tr>
          <!-- Query untuk Mengambil data kriteria pada tabel kriteriasaw dengan memanggil fungsi get_data_kriteria di file saw -->
          <?php
            $kriteria = $saw->get_data_kriteria();
            /*Perulangan untuk menampilkan kriteria yang diumpamakan sebagai K1, K2, dan seterusnya*/
            while ($data_kriteria = $kriteria->fetch(PDO::FETCH_ASSOC)) { ?>
              <!-- Menampilkan Data Kriteria dengan id 1 sebagai K1, id 2 sebagai K2 dan seterusnya -->
              <th>K<?php echo $data_kriteria['id_kriteria']; ?></th>
          <?php } ?>
        </tr>

        <?php
        /*Mencari nilai tertinggi/max dari tiap kriteria dengan inisialisasi misal usia sebagai maxK1, berat badan sebagai maxK2, dan seterusnya*/
        $max = $saw->get_data_max()->fetch(PDO::FETCH_ASSOC);
        /*Mencari nilai terendah/min dari tiap kriteria dengan inisialisasi misal usia semagai minK1, berat badan sebagai minK2, dan seterusnya*/
        $min = $saw->get_data_min()->fetch(PDO::FETCH_ASSOC);

        /*Query untuk Mengambil data dari tabel perkembangan, tabel imunisasi dan tabel balita dengan memanggil fungsi get_data_balita di file saw*/
        $balita = $saw->get_data_balita_normalisasi();
        /*Perulangan untuk menampilkan data balita dengan nilai kriteria masing-masing*/
        while ($data_balita = $balita->fetch(PDO::FETCH_ASSOC)) {
        ?>
				</thead>
        <tr>
          <td>
            <!-- Menampilkan Data Balita dengan id 1 sebagai A1, id 2 sebagai A2 dan seterusnya -->
            <center>A<?php echo $data_balita['idbalita']; ?></center>
          </td>
          <?php
            /*Query untuk Mengambil data kriteria pada tabel kriteriasaw dengan memanggil fungsi get_data_kriteria di file saw*/
            $kriteria = $saw->get_data_kriteria();
            /*variabel noK Untuk menyimpan nomor kriteria*/
            $noK = 0;
            /*Perulangan untuk menampilkan value normalisasi di tiap kriteria*/
            while ($data_kriteria = $kriteria->fetch(PDO::FETCH_ASSOC)) {
              /*variabel noK akan bertambah 1 di awal setiap terjadi perulangan*/
              $noK++;
              /*Mengecek apakah jenis kriteria adalah benefit*/
              $paramKriteria = $saw->get_kategorial($data_balita[$data_kriteria['nama_parameter']], $noK);
              $maxKriteria = 3;
              $minKriteria = 1;
              if ($data_kriteria['jenis'] == "benefit"){ ?>
                <!-- Menampilkan hasil perhitungan normalisasi dengan nilai balita di tiap kriteria dibagi dengan max value dari tiap kriteria yang kemudian dibulatkan dengan presisi 2 yaitu 2 nominal dibelakang koma -->
              <td>
                <center><?php echo round($paramKriteria/$maxKriteria,2); ?></center>
              </td>
            <?php } else if ($data_kriteria['jenis'] == "cost"){ 
            ?>
            <!-- Menampilkan hasil perhitungan normalisasi dengan nilai balita di tiap kriteria dibagi dengan min value dari tiap kriteria yang kemudian dibulatkan dengan presisi 2 yaitu 2 nominal dibelakang koma -->
              <td>
                <center><?php echo round($paramKriteria/$minKriteria,2); ?></center>
              </td>
          <?php }}} ?>
        </tr>
    </table>

    <hr>
    <br><br>

    <!-- Tabel Pembobotan -->
    <h2>Pembobotan</h2>
    <table class="table table-striped table-hover table-sm table-bordered">
      <thead class="thead">

        <tr align="center">
          <th rowspan="2">Balita</th>
          <th colspan="<?php echo $jml_kriteria; ?>">Kriteria</th>
        </tr>

        </tr>

        <tr>
          <!-- Query untuk Mengambil data kriteria pada tabel kriteriasaw dengan memanggil fungsi get_data_kriteria di file saw -->
          <?php
            $kriteria = $saw->get_data_kriteria();
            /*Perulangan untuk menampilkan kriteria yang diumpamakan sebagai K1, K2, dan seterusnya*/
            while ($data_kriteria = $kriteria->fetch(PDO::FETCH_ASSOC)) { ?>
              <!-- Menampilkan Data Kriteria dengan id 1 sebagai K1, id 2 sebagai K2 dan seterusnya -->
              <th>K<?php echo $data_kriteria['id_kriteria']; ?></th>
          <?php } ?>
        </tr>

        <?php
          /*Query untuk Mengambil data dari tabel perkembangan, tabel imunisasi dan tabel balita dengan memanggil fungsi get_data_perkembangan_pembobotan dan disini value yang dipakai adalah status_imunisasi sebagai noK1, status_berat_badan sebagai noK2, dan seterusnya di file saw*/
          $pembobotan = $saw->get_data_perkembangan_pembobotan();
          while ($data_pembobotan = $pembobotan->fetch(PDO::FETCH_ASSOC)) { ?>
    				</thead>
            <tr>
              <td>
                <!-- Menampilkan Data Balita dengan id 1 sebagai A1, id 2 sebagai A2 dan seterusnya -->
                <center>A<?php echo $data_pembobotan['idbalita']; ?></center>
              </td>
              <?php
                /*Query untuk Mengambil data kriteria pada tabel kriteriasaw dengan memanggil fungsi get_data_kriteria di file saw*/
                $kriteria = $saw->get_data_kriteria();
                /*variabel no Untuk menyimpan nomor kriteria*/
                $no = 0;
                while ($data_kriteria = $kriteria->fetch(PDO::FETCH_ASSOC)) {
                  /*variabel no akan bertambah 1 di awal setiap terjadi perulangan*/
                  $no++;
                ?>
                <td>
                  <!-- Menampilkan hasil Pembobotan balita di tiap kriteria, tetapi masih dalah bentuk String, maka jika ingin ditampilkan dalam bentuk angka harus di kategorial terlebih dulu dengan memanggil fungsi get_kategorial di file saw dengan parameter status di tiap kategori dan nomor kategori -->
                  <center><?php 
                    echo $saw->get_kategorial($data_pembobotan[$data_kriteria['nama_parameter']], $no);
                  ?></center>
                </td>
        <?php }} ?>
        </tr>
    </table>

    <hr>
    <br><br>

    <!-- Tabel untuk Perangkingan -->
    <h2>Perankingan</h2>
    <table class="table table-striped table-hover table-sm table-bordered">
      <thead class="thead">
        <tr align="center">
          <th>Ranking</th>
          <th>Nama Balita</th>
          <th>Jumlah</th>
          <th>SAW</th>
          <th>Keterangan</th>
        </tr>
      </thead>

      <tbody>
        <?php
        /*Query untuk Mengambil data dari tabel perkembangan, tabel imunisasi dan tabel balita dengan memanggil fungsi get_data_balita di file saw*/
        $balita = $saw->get_data_balita_perangkingan();
        $no = 1;
        /*Perulangan untuk menampilkan data balita*/
        while ($data_balita = $balita->fetch(PDO::FETCH_ASSOC)) {
            /*Jumlah value balita jika disetiap kategori ditambahkan*/
            $jumlah= 0;

            /*Query untuk Mengambil data kriteria pada tabel kriteriasaw dengan memanggil fungsi get_data_kriteria di file saw*/
            $kriteria = $saw->get_data_kriteria();
            $noK = 0;
            $poin = 0;

            /*Perulangan untuk menghitung Poin SAW di tiap kategori dengan rumus (Poin + Normalisasi x Bobot)*/
            while ($data_kriteria = $kriteria->fetch(PDO::FETCH_ASSOC)) {
              $noK++;
              $paramKriteria = $saw->get_kategorial($data_balita[$data_kriteria['nama_parameter']], $noK);
              $maxKriteria = 3;
              $minKriteria = 1;

              /*Cek apakah kriteria berjenis benefit*/
              if ($data_kriteria['jenis'] == "benefit"){
                /*Menghitung Poin SAW dengan rumus (Poin + Normalisasi x Bobot)*/
                $poin = $poin + (($paramKriteria/$maxKriteria)*$data_kriteria['bobot']);
              } 
              /*Cek apakah kriteria berjenis benefit*/
              else if ($data_kriteria['jenis' == "cost"]){
                /*Menghitung Poin SAW dengan rumus (Poin + Normalisasi x Bobot)*/
                $poin = $poin + (($paramKriteria/$minKriteria)*$data_kriteria['bobot']);
              }

              $jumlah = $jumlah + $paramKriteria;
            }
            /*Poin SAW dibulatkan dengan presisi 3, yaitu terdapat 3 nominal dibelakang koma*/
            $poin = round($poin, 3);
            /*$poin= round(
             (($data_balita['usia']/$max['maxK1'])*$bobot[0])+
             (($data_balita['berat_badan']/$max['maxK2'])*$bobot[1])+
             (($data_balita['tinggi_badan']/$max['maxK3'])*$bobot[2])+
             (($data_balita['lingkar_kepala']/$max['maxK4'])*$bobot[3]),3);*/

             /*Menyimpan data nama balita, jumlah value balita, dan Poin SAW di tiap balita ke dalam array */
            $data[]=array('nama'=>$data_balita['nama_balita'],
              'jumlah'=>$jumlah,
              'poin'=>$poin);
        }

        /*Perulangan pada array data untuk menampilkan perangkingan balita*/
        foreach ($data as $key => $isi) {
          /*Menyimpan nama, jumlah value, dan poin saw di tiap balita*/
          $nama[$key]=$isi['nama'];
          $jlh[$key]=$isi['jumlah'];
          $poin1[$key]=$isi['poin'];
         }
         /*Mensortir perangkingan berdasarkan poin yang lebih besar dipaling diutamakan yang kemudian baru jumlah value, jadi misal jumlah saw nya dari 2 balita itu sama, maka akan dilihat jumlah value balita mana yang lebih besar maka akan ditaruh diatas dari jumlah value balita yang lebih kecil*/
         array_multisort($poin1,SORT_DESC,$jlh,SORT_DESC,$data);
         $ranking=0;
         $ket="";

         /*Untuk mengkategorisasi keterangan berdasarkan rangking*/
         foreach ($data as $item) {
          $ranking++;
          $double = doubleval($item['poin']);
          if ($double >= 0.750){
            $ket = "Sehat";
          } else {
            $ket = "Kurang Sehat";
          }
        ?>
        <tr>
          <td>
            <center><?php echo $ranking; ?></center>
          </td>
          <td>
            <center><?php echo $item['nama']; ?></center>
          </td>
          <td>
            <center><?php echo $item['jumlah']; ?></center>
          </td>
          <td>
            <center><?php echo$item['poin']; ?></center>
          </td>
          <td>
            <center><?php echo"$ket" ?></center>
          </td>

        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>

  <?php
		  include('template/footer.php');
	  ?>
</body>

</html>
