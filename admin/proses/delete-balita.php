<?php

include('../koneksi.php');
 
/*Jika saat file ini dipanggil dan membawa method GET dengan idbalita maka akan melanjutkan proses query ke database*/
if(isset($_GET['idbalita'])){
	/*Menyimpan data idbalita yang dibawah oleh GET ke variabel*/
	$idbalita = $_GET['idbalita'];
	
	/*Mengecek di database tabel balita apakah idbalita yang dibawah oleh GET terdaftar di database*/
	$cek = mysqli_query($koneksi, "SELECT * FROM balita WHERE idbalita='$idbalita'") or die(mysqli_error($koneksi));
	
	/*Jika idbalita yang dibawa oleh GET terdaftar di database, maka akan melanjutkan proses menghapus*/
	if(mysqli_num_rows($cek) > 0){
		/*Query database untuk menghapus data balita sesuai idbalita yang dibawa oleh GET*/
		$del = mysqli_query($koneksi, "DELETE FROM balita WHERE idbalita='$idbalita'") or die(mysqli_error($koneksi));

		/*Jika menghapus data berhasil, maka akan pindah halaman ke index-balita.php dan menampilkan alert bahwa berhasil untuk menghapus data*/
		if($del){
			echo '<script>alert("Berhasil menghapus data."); document.location="../index-balita.php";</script>';
		}
		/*Jika menghapus data gagal, maka akan menampilkan alert bahwa gagal untuk menghapus data dan akan balik ke halaman index-balita.php*/
		else{

			echo '<script>alert("Gagal menghapus data."); document.location="../index-balita.php";</script>';
		}
	/*Jika idbalita tidak terdaftar di database, maka akan menampilkan alert bahwa id balita tidak ditemukan*/
	}else{
		echo '<script>alert("ID tidak ditemukan di database."); document.location="../index-balita.php";</script>';
	}
}
 
?>
