<?php
	include('../koneksi.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Title tab bar di web -->
	<title>Posyandu Anggrek II Mayungan</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- import bootstrap, JQuery, dan style.css -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<style>
		.navbar-nav .nav-item:not(:last-child) {
			border-right: 1px solid silver;
		}

		@media (max-width: 768px) {
			.navbar-nav .nav-item:not(:last-child) {
				border-right: none;
			}
		}

		.navbar-brand {
			font-family: 'Roboto', sans-serif;
			font-size: 25px;
			margin: 0px;
		}
	</style>
</head>

<body>
	<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
		<!-- Logo -->
        <a class="navbar-brand">
            <img src="http://indihealth.com/indihealthcom/assets/images/products/5acf891ba61c4.png" alt="Logo"
                style="width:150px;"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Navigasi bar -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav ml-auto">
            	<!-- Navigasi untuk dropdown Data Balita -->
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Data Balita
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../index-balita.php">Data Umum</a>
                        <a class="dropdown-item" href="../index-imunisasi.php">Data Imunisasi</a>
                        <a class="dropdown-item" href="../kriteria.php">Data Kriteria</a>
                    </div>
                </li>
                <!-- Navigasi ke Data Perkembangan -->
                <li class="nav-item">
                    <a class="nav-link" href="../index-perkembangan.php">Data Perkembangan</a>
                </li>
                <!-- Navigasi ke Laporan Perkembangan -->
                <li class="nav-item">
                    <a class="nav-link" href="../laporan-perkembangan.php">Laporan Perkembangan</a>
                </li>
            </ul>
        </div>
    </nav>

	<br>

	<div class="container">
		<!-- Title -->
		<h2>Tambah Data Imunisasi Balita</h2>
		<hr>

		<!-- Cek apakah membawa parameter untuk post -->
		<?php
		if(isset($_POST['submit'])){
			/*Jika membawa parameter untuk post maka menyimpan data dari post ke variable-variable sesuai nama parameter nya*/
			$idbalita		= $_POST['idbalita'];
			$hb             = $_POST['hb'];
			$bcg	        = $_POST['bcg'];
			$dpt1	        = $_POST['dpt1'];
            $hb2	        = $_POST['hb2'];
            $hb3	        = $_POST['hb3'];
            $campak	        = $_POST['campak'];
            $status_imunisasi	= $_POST['status_imunisasi'];
            $status_berat_badan	= $_POST['status_berat_badan'];
            $status_tinggi_badan	= $_POST['status_tinggi_badan'];
            $status_lingkar_kepala	= $_POST['status_lingkar_kepala'];

			/*SQL Query untuk manambah data yang dibawa oleh POST ke database mysql ke tabel imunisasi*/
			$sql = mysqli_query($koneksi, "INSERT INTO imunisasi(idbalita, hb, bcg, dpt1, hb2, hb3, campak, status_imunisasi, status_berat_badan, status_tinggi_badan, status_lingkar_kepala)
				VALUES('$idbalita', '$hb', '$bcg', '$dpt1', '$hb2', '$hb3', '$campak', '$status_imunisasi', '$status_berat_badan', '$status_tinggi_badan', '$status_lingkar_kepala')") or die(mysqli_error($koneksi));

					/*Jika proses penambahan datanya berhasil, maka akan pindah ke halaman imunisasi dan menampilkan alert bahwa berhasil menambahkan data*/
					if($sql){
						echo '<script>alert("Berhasil menambahkan data."); document.location="../index-imunisasi.php";</script>';
					}else{
						/*Jika gagal menambahkan data ke database, maka akan menampilkan alert bahwa gagal untuk melakukan proses tambah data*/
						echo '<div class="alert alert-warning">Gagal melakukan proses tambah data.</div>';
					}
			}
		?>

		<!-- Form untuk menambah data imunisasi dengan method post dan action saat submit menuju ke create-imunisasi.php -->
		<form action="create-imunisasi.php" method="post" autocomplete="off">
			<!-- Form untuk memilih Nama Balita dengan menggunakan dropdown, jadi harus mengambil data dari database untuk menampilkan dropdown nama balita -->
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Nama Balita</label>
				<div class="col-sm-10">
					<select id="idbalita" class="form-control" name="idbalita">
						<!-- Query untuk mengambil data balita -->
						<?php 
							$sql = mysqli_query($koneksi,"SELECT * FROM balita");
							/*Perulangan untuk menampilkan nama balita*/
							while ($result = mysqli_fetch_array($sql)) {
						?>
						<!-- Menampilkan nama balita -->
						<option value="<?php echo $result['idbalita'] ?>"><?php echo $result['nama_balita'] ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">HB</label>
				<div class="col-sm-10">
					<input type="date" name="hb" class="form-control">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">BCG</label>
				<div class="col-sm-10">
					<input type="date" name="bcg" class="form-control">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">DPT 1</label>
				<div class="col-sm-10">
					<input type="date" name="dpt1" class="form-control">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">HB 2</label>
				<div class="col-sm-10">
					<input type="date" name="hb2" class="form-control">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">HB 3</label>
				<div class="col-sm-10">
					<input type="date" name="hb3" class="form-control">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Campak</label>
				<div class="col-sm-10">
					<input type="date" name="campak" class="form-control">
				</div>
			</div>
			<!-- Form untuk dropdown status kelengkapan -->
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Status Kelengkapan</label>
				<div class="col-sm-10">
					<select class="form-control" name="status_imunisasi" required>
						<option value=""> Pilih Status </option>
						<option value="Lengkap">Lengkap</option>
						<option value="Kurang Lengkap">Kurang Lengkap</option>
						<option value="Tidak Ada">Tidak Ada</option>
					</select>
				</div>
			</div>
			<!-- Form untuk dropdown status berat badan -->
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Status Berat Badan</label>
				<div class="col-sm-10">
					<select class="form-control" name="status_berat_badan" required>
						<option value=""> Pilih Status </option>
						<option value="Naik">Naik</option>
						<option value="Tetap">Tetap</option>
						<option value="Turun">Turun</option>
					</select>
				</div>
			</div>
			<!-- Form untuk dropdown status tinggi badan -->
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Status Tinggi Badan</label>
				<div class="col-sm-10">
					<select class="form-control" name="status_tinggi_badan" required>
						<option value=""> Pilih Status </option>
						<option value="Naik">Naik</option>
						<option value="Tetap">Tetap</option>
						<option value="Turun">Turun</option>
					</select>
				</div>
			</div>
			<!-- Form untuk dropdown status lingkar kepala -->
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Status Lingkar Kepala</label>
				<div class="col-sm-10">
					<select class="form-control" name="status_lingkar_kepala" required>
						<option value=""> Pilih Status </option>
						<option value="Baik">Baik</option>
						<option value="Cukup">Cukup</option>
						<option value="Kurang Baik">Kurang Baik</option>
					</select>
				</div>
			</div>
			<div class="form-group text-right">
				<a href="../index-imunisasi.php" class="btn btn-default">Kembali</a>
				<button type="submit" name="submit" class="btn btn-primary">Simpan</button>
			</div>
		</form>
	</div>
</body>

</html>

<html>
