<?php
	include('../koneksi.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Title tab bar di web -->
    <title>Posyandu Anggrek II Mayungan</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- import bootstrap, JQuery, dan style.css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .navbar-nav .nav-item:not(:last-child) {
            border-right: 1px solid silver;
        }

        @media (max-width: 768px) {
            .navbar-nav .nav-item:not(:last-child) {
                border-right: none;
            }
        }

        .navbar-brand {
            font-family: 'Roboto', sans-serif;
            font-size: 25px;
            margin: 0px;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
        <!-- Logo -->
        <a class="navbar-brand">
            <img src="http://indihealth.com/indihealthcom/assets/images/products/5acf891ba61c4.png" alt="Logo"
                style="width:150px;"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Navigasi bar -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav ml-auto">
                <!-- Navigasi untuk dropdown Data Balita -->
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Data Balita
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="../index-balita.php">Data Umum</a>
                        <a class="dropdown-item" href="../index-imunisasi.php">Data Imunisasi</a>
                        <a class="dropdown-item" href="../kriteria.php">Data Kriteria</a>
                    </div>
                </li>
                <!-- Navigasi ke Data Perkembangan -->
                <li class="nav-item">
                    <a class="nav-link" href="../index-perkembangan.php">Data Perkembangan</a>
                </li>
                <!-- Navigasi ke Laporan Perkembangan -->
                <li class="nav-item">
                    <a class="nav-link" href="../laporan-perkembangan.php">Laporan Perkembangan</a>
                </li>
            </ul>
        </div>
    </nav>

    <br>

    <div class="container">
        <!-- Title -->
        <h2>Edit Data Balita</h2>
        <hr>

        <!-- Cek apakah membawa parameter untuk GET dengan membawa idbalita -->
        <?php
		if(isset($_GET['idbalita'])){
            /*Menyimpan idbalita yang dibawa oleh GET*/
			$idbalita = $_GET['idbalita'];
			
            /*mengambil data balita dari mysql database berdasarkan idbalita yang dibawa oleh GET*/
			$select = mysqli_query($koneksi, "SELECT * FROM balita WHERE idbalita='$idbalita'") or die(mysqli_error($koneksi));

            /*Jika data balita tidak terdaftar maka akan menampilkan alert bahwa balita tidak ada dalam database*/
			if(mysqli_num_rows($select) == 0){
				echo '<div class="alert alert-warning">ID tidak ada dalam database.</div>';
				exit();
			}
            /*Jika data balita terdaftar maka akan menyimpan data balita tersebut di variabel data*/
            else{
				$data = mysqli_fetch_assoc($select);
			}
		}
		?>

        <!-- Cek apakah membawa parameter untuk POST -->
        <?php
		if(isset($_POST['submit'])){
            /*Jika membawa parameter untuk post maka menyimpan data dari post ke variable-variable sesuai nama parameter nya*/
			$idbalita		= $_POST['idbalita'];
            $nama_balita	= $_POST['nama_balita'];
            $tanggal_lahir	= $_POST['tanggal_lahir'];
            $jenis_kelamin	= $_POST['jenis_kelamin'];
            $nama_ayah		= $_POST['nama_ayah'];
            $nama_ibu		= $_POST['nama_ibu'];
            $alamat		    = $_POST['alamat'];
			
            /*Query untuk mengupdate data balita*/
			$sql = mysqli_query($koneksi, "UPDATE balita SET idbalita='$idbalita', nama_balita='$nama_balita', 
                tanggal_lahir='$tanggal_lahir', jenis_kelamin='$jenis_kelamin', nama_ayah='$nama_ayah',
                nama_ibu='$nama_ibu', alamat='$alamat' WHERE idbalita='$idbalita'") or die(mysqli_error($koneksi));
			
            /*Jika query update data balita berhasil maka akan ganti halaman ke index-balita.php dan menampilkan alert bahwa berhasil memperbarui data*/
			if($sql){
				echo '<script>alert("Berhasil memperbarui data."); document.location="../index-balita.php?idbalita='.$idbalita.'";</script>';
			}
            /*Jika query update data balita gagal, maka akan menampilkan alert bahwa proses edit data balita gagal*/
            else{
				echo '<div class="alert alert-warning">Gagal melakukan proses edit data.</div>';
			}
		}
		?>

        <!-- Form untuk mengedit data balita dengan method post dan action saat submit menuju ke edit-balita.php -->
        <form action="edit-balita.php?idbalita=<?php echo $idbalita; ?>" method="post" autocomplete="off">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">ID Balita</label>
                <div class="col-sm-10">
                    <input type="text" name="idbalita" class="form-control" value="<?php echo $data['idbalita']; ?>"
                        required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama balita</label>
                <div class="col-sm-10">
                    <input type="text" name="nama_balita" class="form-control"
                        value="<?php echo $data['nama_balita']; ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Tanggal Lahir</label>
                <div class="col-sm-10">
                    <input type="date" name="tanggal_lahir" class="form-control"
                        value="<?php echo $data['tanggal_lahir']; ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <input type="radio" class="form-check-input" name="jenis_kelamin" value="Laki-laki"
                            <?php if($data['jenis_kelamin'] == 'Laki-laki'){ echo 'checked'; } ?> required>
                        <label class="form-check-label">Laki-laki</label>
                    </div>
                    <div class="form-check">
                        <input type="radio" class="form-check-input" name="jenis_kelamin" value="Perempuan"
                            <?php if($data['jenis_kelamin'] == 'Perempuan'){ echo 'checked'; } ?> required>
                        <label class="form-check-label">Perempuan</label>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama Ayah</label>
                <div class="col-sm-10">
                    <input type="text" name="nama_ayah" class="form-control" value="<?php echo $data['nama_ayah']; ?>"
                        required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama Ibu</label>
                <div class="col-sm-10">
                    <input type="text" name="nama_ibu" class="form-control" value="<?php echo $data['nama_ibu']; ?>"
                        required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Alamat</label>
                <div class="col-sm-10">
                    <input type="text" name="alamat" class="form-control" value="<?php echo $data['alamat']; ?>"
                        required>
                </div>
            </div>
            <div class="form-group text-right">
                <a href="../index-balita.php" class="btn btn-default">Kembali</a>
                <button type="submit" name="submit" class="btn btn-primary">Perbarui</button>
            </div>
        </form>

    </div>
</body>

</html>
